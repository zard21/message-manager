package com.zard21.msg;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class HeungGukProcessor {

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
        String currentDate = now.format(dtf);

        List<Object> header = new ArrayList<>();

        header.add("184");
        header.add("CLOB0000");
        header.add(currentDate);
        header.add("4");
        header.add("홍길동");
        header.add("7705011");
        header.add("01045872987");

//        String headerStr = MessageProcessor.generateMessage("HG_HEADER", header);

        List<Object> body = new ArrayList<>();

        body.add("홍길동");
        body.add("197705");
        body.add("1");
        body.add("010");
        body.add("45872987");
        body.add("");
        body.add("");
        body.add("");
        body.add("");

//        String bodyStr = MessageProcessor.generateMessage("CLOB0000", body);

//        System.out.println("[" + headerStr + bodyStr + "]");
    }
}

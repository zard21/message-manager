package com.zard21.msg.util;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

public class MessageUtil {

    public final static String YAML_EXTENSION       = ".yml";

    public static int getMessageLength(String telegramId) {
        Yaml yaml = new Yaml();
        ClassLoader loader = new MessageUtil().getClass().getClassLoader();

        int length = 0;

        try (InputStream inputStream = new FileInputStream(
                new File(loader.getResource(telegramId + YAML_EXTENSION).getFile()))) {
            Object data = yaml.load(inputStream);
            length = Integer.parseInt(((Map)data).get("length").toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return length;
    }
}

package com.zard21.msg.model;

import lombok.Data;

@Data
public class ACE0001 {

    private Header header;
    private Contract contract;
    private RelationInfo relationInfo;
    private Result result;

    @Data
    public class Header {
        private String sponsorCode;
        private String sponsorMngNo;
        private String serviceType;
        private String action;
    }

    @Data
    public class Contract {
        private String productType;
        private String policyStartDate;
        private String policyStartHour;
        private String policyEndDate;
        private String policyEndHour;
        private String travelPlace;
        private String appPremium;
    }

    @Data
    public class RelationInfo {
        private String relatorSeq;
        private String relatorType;
        private String relationHolder;
        private String relatorName;
        private String relatorEngName;
        private String relatorPlanType;
        private String relatorBirthDate;
        private String relatorSexCode;
    }

    @Data
    public class Result {
        private String returnCode;
        private String returnMsg;
    }
}

package com.zard21.msg.model;

import com.zard21.msg.annotation.MessageDef;
import lombok.Data;

@Data
public class UserInfo {

    @MessageDef(length = 3, type = "number", desc = "User's age")
    int age;

    @MessageDef(length = 20, type = "text", desc = "User's name")
    String name;

    @MessageDef(length = 20, type = "text", desc = "User's mobile phone number")
    String phone;

    @MessageDef(length = 50, type = "text", desc = "User's email address")
    String email;
}

package com.zard21.msg;

import java.nio.charset.Charset;

public class LengthTest {

    public static void main(String[] args) {

        String str = "김응주   ";

        System.out.println(str.getBytes().length);
        System.out.println(str.getBytes(Charset.forName("euc-kr")).length);
    }
}

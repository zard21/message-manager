package com.zard21.msg;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.Yaml;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class YmlWriter {

    public static void main(String[] args) {
        String fileName = "/Users/zard21/development/IdeaProjects/message-manager/src/main/resources/test_info.yml";

        DumperOptions options = new DumperOptions();
        options.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
        options.setPrettyFlow(true);
        Yaml yaml = new Yaml(options);

        List<Map<String, Object>> list = new ArrayList<>();

        Map<String, Object> map;

        map = new HashMap<>();
        map.put("name", "age");
        map.put("type", "number");
        map.put("length", "20");
        list.add(map);

        map = new HashMap<>();
        map.put("name", "name");
        map.put("type", "text");
        map.put("length", "40");
        list.add(map);

        map = new HashMap<>();
        map.put("name", "phone");
        map.put("type", "text");
        map.put("length", "20");
        list.add(map);

        map = new HashMap<>();
        map.put("name", "salary");
        map.put("type", "number");
        map.put("length", "30");
        list.add(map);

        map = new HashMap<>();
        map.put("name", "email");
        map.put("type", "text");
        map.put("length", "30");
        list.add(map);

        Map<String, Object> finalMap = new HashMap<>();
        finalMap.put("id", "test_info");
        finalMap.put("count", "5");
        finalMap.put("field", list);

        try {
            FileWriter writer = new FileWriter(fileName);
            yaml.dump(finalMap, writer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package com.zard21.msg;

import com.zard21.msg.annotation.MessageDef;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

@SuppressWarnings("WeakerAccess")
public class MessageGenerator {

    private final static String ATTR_LENGTH = "length";
    private final static String ATTR_TYPE = "type";
    private final static String ATTR_TYPE_NUMBER = "number";

    @SuppressWarnings("WeakerAccess")
    public String generateMessage(Object obj) {
        Class<?> cls = obj.getClass();

        Field[] fields = cls.getDeclaredFields();

        StringBuilder resultStr = new StringBuilder();

        String lengthStr = "";
        String typeStr = "";
        String formatStr;

        try {
            for (Field field : fields) {
                if (field.isAnnotationPresent(MessageDef.class)) {
                    field.setAccessible(true);
                    Annotation[] annotations = field.getDeclaredAnnotations();
                    for (Annotation annotation : annotations) {
                        Class<? extends Annotation> type = annotation.annotationType();

                        for (Method method : type.getDeclaredMethods()) {
                            Object value = method.invoke(annotation, (Object[]) null);

                            if (method.getName().equals(ATTR_LENGTH)) {
                                lengthStr = value.toString();
                            } else if (method.getName().equals(ATTR_TYPE)) {
                                typeStr = value.toString();
                            }
                        }

                        if (typeStr.equals(ATTR_TYPE_NUMBER)) {
                            formatStr = String.format("%0" + lengthStr + "d", Integer.parseInt(field.get(obj).toString()));
                        } else {
                            formatStr = String.format("%-" + lengthStr + "s", field.get(obj).toString());
                        }

                        resultStr.append(formatStr);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultStr.toString();
    }
}

package com.zard21.msg;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zard21.msg.model.ACE0001;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class MainApplication {

    public static void main(String[] args) {

        List<Object> msg = new ArrayList<>();

        // header
        msg.add("700001SAMD");
        msg.add("20180630_1");
        msg.add("01");
        msg.add("01");

        // contract
        msg.add("PR001");
        msg.add("20180701");
        msg.add("1410");
        msg.add("20180703");
        msg.add("1800");
        msg.add("US");
        msg.add("10500");

        // relationInfo
        msg.add("1");
        msg.add("02");
        msg.add("01");
        msg.add("홍길동");
        msg.add("HongGilDong");
        msg.add("PL001");
        msg.add("19881210");
        msg.add("1");

        // result
        msg.add("");
        msg.add("");

        ObjectMapper mapper = new ObjectMapper();

        ACE0001 ace0001 = new ACE0001();

        ACE0001.Header header = ace0001.new Header();
        header.setSponsorCode("700001SAMD");
        header.setSponsorMngNo("20180630_1");
        header.setServiceType("01");
        header.setAction("01");
        ace0001.setHeader(header);

        ACE0001.Contract contract = ace0001.new Contract();
        contract.setProductType("PR001");
        contract.setPolicyStartDate("20180701");
        contract.setPolicyStartHour("1410");
        contract.setPolicyEndDate("20180703");
        contract.setPolicyEndHour("1800");
        contract.setTravelPlace("US");
        contract.setAppPremium("10500");
        ace0001.setContract(contract);

        ACE0001.RelationInfo relationInfo = ace0001.new RelationInfo();
        relationInfo.setRelatorSeq("1");
        relationInfo.setRelatorType("02");
        relationInfo.setRelationHolder("01");
        relationInfo.setRelatorName("홍길동");
        relationInfo.setRelatorEngName("HongGilDong");
        relationInfo.setRelatorPlanType("PL001");
        relationInfo.setRelatorBirthDate("19881210");
        relationInfo.setRelatorSexCode("1");
        ace0001.setRelationInfo(relationInfo);

        ACE0001.Result result = ace0001.new Result();
        result.setReturnCode("");
        result.setReturnMsg("");
        ace0001.setResult(result);

        try {
            String jsonStr = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(ace0001);

            ACE0001 resAce0001 = mapper.readValue(jsonStr, ACE0001.class);

            System.out.println(jsonStr);
            System.out.println(resAce0001.getHeader().getSponsorCode());
            System.out.println(resAce0001.getHeader().getSponsorMngNo());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

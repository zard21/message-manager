package com.zard21.msg;

import com.zard21.msg.util.MessageUtil;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MessageProcessor {

    private final static String ATTR_TYPE_NUMBER = "number";
    private final static String ATTR_TYPE_TEXT = "text";
    private final static String ATTR_TYPE_LIST = "list";
    private final static String YAML_EXTENSION = ".yml";

    private final static String filePath =
            "/Users/zard21/development/IdeaProjects/message-manager/src/main/resources/";

    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String currentTime = now.format(dtf);

//        List<Object> header = new ArrayList<>();
//
//        header.add("MSG001");
//        header.add("1");
//        header.add(currentTime);
//
//        List<Object> messages = new ArrayList<>();
//
//        messages.add("10");
//        messages.add("KIM");
//        messages.add("010-1234-5678");
//        messages.add("50000000");
//        messages.add("mail@gmail.com");
//
//        List<Object> subMessages = new ArrayList<>();
//
//        List<String> subList;
//
//        subList = new ArrayList<>();
//        subList.add("abc");
//        subList.add("3000");
//        subMessages.add(subList);
//
//        subList = new ArrayList<>();
//        subList.add("def");
//        subList.add("50000");
//        subMessages.add(subList);
//
//        subList = new ArrayList<>();
//        subList.add("ghi");
//        subList.add("7000");
//        subMessages.add(subList);
//
//        messages.add(subMessages);

        List<Object> header = new ArrayList<>();

        header.add("184");
        header.add("CLOB0000");
        header.add("20180401");
        header.add("4");
        header.add("홍길동");
        header.add("7705011");
        header.add("01012345678");

        List<Object> body = new ArrayList<>();

        body.add("홍길동");
        body.add("770501");
        body.add("2");
        body.add("010");
        body.add("12345678");
        body.add("");
        body.add("");
        body.add("");
        body.add("");

//        byte[] resultBytes = new byte[0];
//        resultBytes = appendBytes(resultBytes, generateMessage("HG_HEADER", header));
//        resultBytes = appendBytes(resultBytes, generateMessage("CLOB0000", body));
//        System.out.println(resultBytes.length);
//        System.out.println("[" + new String(resultBytes, Charset.forName("euc-kr")) + "]");

        System.out.println(MessageUtil.getMessageLength("CLOB0000"));
        String str = "00000184CLOB0000201804014       홍길동              770501101012345678홍길동              770501201012345678                                                                                    ";

        byte[] msg = str.getBytes(Charset.forName("euc-kr"));
        byte[] headerMsg = new byte[70];
        byte[] bodyMsg = new byte[122];
        System.arraycopy(msg, 0, headerMsg, 0, 70);
        System.arraycopy(msg, 70, bodyMsg, 0, 122);

        List<Object> result = parseMessage("HG_HEADER", headerMsg);
        System.out.println(">> " + result);

        result = parseMessage("CLOB0000", bodyMsg);
        System.out.println(">> " + result);

//        System.out.println(headerStr.getBytes(Charset.forName("euc-kr")).length);
//        System.out.println(bodyStr.length());

//        System.out.println("Original Message : " + messages);

//        String headerStr = generateMessage("header", header);
//        String generatedStr = new String(generateMessage("user_info", messages));

//        System.out.println("Header: [" + headerStr + "]");
//        System.out.println("Generated Message: [" + generatedStr + "]");

//        List<Object> result = parseMessage("user_info", generatedStr);
//        System.out.println("Parsed Message   : " + result);

//        printValue(result);
    }

    public static void printValue(List<Object> result) {
        for (Object data : result) {
            if (data instanceof ArrayList) {
                printValue((List<Object>)data);
            } else {
                System.out.println(data.toString());
            }
        }
    }

    public static List<Object> parseMessage(String telegramId, byte[] message) {
        Yaml yaml = new Yaml();
        Map<String, Object> map;
        List<Object> parseResult = new ArrayList<>();

        int offset = 0;

        try (InputStream inputStream = new FileInputStream(new File(filePath + telegramId + YAML_EXTENSION))) {
            for (Object data : yaml.loadAll(inputStream)) {
                List<Map<String, String>> list = (ArrayList)((Map)data).get("field");

                for (int i = 0; i < list.size(); i++) {
                    map = new HashMap<>(list.get(i));

                    if (map.get("type").equals(ATTR_TYPE_NUMBER)) {
                        parseResult.add(
                                new String(message, offset, Integer.parseInt(map.get("length").toString()))
                                    .replaceFirst("^0+", ""));
                    } else if (map.get("type").equals(ATTR_TYPE_TEXT)) {
                        parseResult.add(
                                new String(message, offset, Integer.parseInt(map.get("length").toString()), Charset.forName("euc-kr"))
                                    .replaceFirst("\\s+$", ""));
                    } else if (map.get("type").equals(ATTR_TYPE_LIST)) {
                        int subFieldCount = Integer.parseInt(map.get("length").toString());
                        List<Map<String, String>> subList = (ArrayList)map.get("list_field");

                        List<Object> resArray = new ArrayList<>();

                        for (int j = 0; j < subFieldCount; j++) {
                            List<String> subArray = new ArrayList<>();

                            for (int k = 0; k < subList.size(); k++) {
                                Map<String, String> subRowData = subList.get(k);

                                String tmpStr = new String(message, offset, Integer.parseInt(subRowData.get("length")), Charset.forName("euc-kr"));

                                if (subRowData.get("type").equals(ATTR_TYPE_NUMBER)) {
                                    if (!tmpStr.trim().equals(""))
                                        subArray.add(tmpStr.replaceFirst("^0*", ""));
                                } else {
                                    if (!tmpStr.trim().equals(""))
                                        subArray.add(tmpStr.trim());
                                }

                                offset += Integer.parseInt(subRowData.get("length"));
                            }

                            if (!subArray.contains(""))
                                resArray.add(subArray);
                        }
                        parseResult.add(resArray);
                    }

                    offset += Integer.parseInt(map.get("length").toString());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return parseResult;
    }

    public static byte[] generateMessage(String telegramId, List<Object> inputMessage) {
        Yaml yaml = new Yaml();
        Map<String, Object> map;

        byte[] resultBytes = new byte[0];

        try (InputStream inputStream = new FileInputStream(new File(filePath + telegramId + YAML_EXTENSION))) {
            for (Object data : yaml.loadAll(inputStream)) {
                Map<String, Object> rowData = (Map)data;

                if (Integer.parseInt(rowData.get("count").toString()) != inputMessage.size()) {
                    System.out.println("Wrong Field Count!!!");
                    break;
                }

                List<Map<String, String>> list = (ArrayList)rowData.get("field");

                for (int i = 0; i < list.size(); i++) {
                    map = new HashMap<>(list.get(i));

                    if (map.get("type").equals(ATTR_TYPE_NUMBER)) {
                        resultBytes = appendBytes(resultBytes,
                                String.format("%0" + map.get("length") + "d", Integer.parseInt(inputMessage.get(i).toString())).getBytes());
                    } else if (map.get("type").equals(ATTR_TYPE_TEXT)) {
                        byte[] inputBytes = inputMessage.get(i).toString().getBytes(Charset.forName("euc-kr"));
                        byte[] tmpBytes = new byte[Integer.parseInt(map.get("length").toString())];

                        System.arraycopy(inputBytes, 0, tmpBytes, 0, inputBytes.length);

                        for (int j = inputBytes.length; j < tmpBytes.length; j++) {
                            tmpBytes[j] = ' ';
                        }

                        resultBytes = appendBytes(resultBytes, tmpBytes);
                    } else if (map.get("type").equals(ATTR_TYPE_LIST)) {
                        int subFieldCount = Integer.parseInt(map.get("length").toString());

                        List<Map<String, String>> subList = (ArrayList)map.get("list_field");
                        List<Object> subData = (List)inputMessage.get(i);

                        // Generate empty message
                        StringBuilder emptyMsg = new StringBuilder();

                        for (int idx = 0; idx < subList.size(); idx++) {
                            if (subList.get(idx).get("type").equals(ATTR_TYPE_NUMBER)) {
                                emptyMsg.append(
                                        String.format("%0" + subList.get(idx).get("length") + "d", 0));
                            } else {
                                emptyMsg.append(
                                        String.format("%-" + subList.get(idx).get("length") + "s", " "));
                            }
                        }

                        for (int j = 0; j < subFieldCount; j++) {
                            if (j < subData.size()) {
                                List<String> subRowData = (List)subData.get(j);
                                for (int k = 0; k < subRowData.size(); k ++) {
                                    if (subList.get(k).get("type").equals(ATTR_TYPE_NUMBER)) {
                                        resultBytes = appendBytes(resultBytes,
                                                String.format("%0" + subList.get(k).get("length") + "d",
                                                        Integer.parseInt(subRowData.get(k))).getBytes());
                                    } else {
                                        resultBytes = appendBytes(resultBytes,
                                                String.format("%-" + subList.get(k).get("length") + "s",
                                                        subRowData.get(k)).getBytes(Charset.forName("euc-kr")));
                                    }
                                }
                            } else {
                                resultBytes = appendBytes(resultBytes, emptyMsg.toString().getBytes());
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resultBytes;
    }

    public static byte[] appendBytes(byte[] a, byte[] b) {
        byte[] result = new byte[a.length + b.length];
        System.arraycopy(a, 0, result, 0, a.length);
        System.arraycopy(b, 0, result, a.length, b.length);

        return result;
    }
}
